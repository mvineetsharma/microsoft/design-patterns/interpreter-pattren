﻿using System;
using System.Collections.Generic;

// Abstract expression interface
public abstract class Expression
{
    public abstract int Interpret(Context context);
}

// Terminal expression for counting fruits
public class CountExpression : Expression
{
    public override int Interpret(Context context)
    {
        return context.GetFruitCount();
    }
}

// Terminal expression for listing fruits
public class ListExpression : Expression
{
    public override int Interpret(Context context)
    {
        List<string> fruits = context.GetFruitList();
        foreach (var fruit in fruits)
        {
            Console.WriteLine(fruit);
        }
        return fruits.Count;
    }
}

// Non-terminal expression for calculating the total number of fruits
public class TotalExpression : Expression
{
    public override int Interpret(Context context)
    {
        return context.GetFruitCount();
    }
}

// Context class to store and manage the state
public class Context
{
    private List<string> fruits = new List<string>();

    public void AddFruit(string fruit)
    {
        fruits.Add(fruit);
    }

    public List<string> GetFruitList()
    {
        return fruits;
    }

    public int GetFruitCount()
    {
        return fruits.Count;
    }
}

class Program
{
    static void Main(string[] args)
    {
        Context context = new Context();
        context.AddFruit("Apple");
        context.AddFruit("Banana");
        context.AddFruit("Orange");

        Expression countExpression = new CountExpression();
        Expression listExpression = new ListExpression();
        Expression totalExpression = new TotalExpression();

        Console.WriteLine($"Number of fruits: {countExpression.Interpret(context)}");
        Console.WriteLine($"List of fruits:");
        listExpression.Interpret(context);
        Console.WriteLine($"Total fruits: {totalExpression.Interpret(context)}");
    }
}
